<?php
    $ret = array();
    $dir = 'img/gallery/';
    if (@$handle = opendir($dir)){
        while (false !== ($file = readdir($handle))){
            if ($file != '.' && $file != '..'){
                $ret[] = '{"url":"' . $dir . $file . '"}';
            }
        }
        closedir($handle);
    }
    header('Content-Type: application/json');
    echo '[' . implode(',', $ret) . ']';
    exit();
?>